﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Map mapa = new Map(12);
            
            
            mapa.cells[1, 1] = true;
            mapa.cells[1, 2] = true;
            mapa.cells[1, 3] = true;
            mapa.cells[2, 1] = true;
            mapa.cells[3, 1] = true;
            mapa.cells[3, 2] = true;
            mapa.cells[4, 1] = true;
            mapa.cells[5, 1] = true;
            mapa.cells[5, 2] = true;
            mapa.cells[5, 3] = true;
            

            mapa.Fill();
            Console.ReadKey();
            while (true)
            {
                Console.Clear();
                mapa.Play();
                mapa.Fill();
                Thread.Sleep(1000);
                //Console.ReadKey();
            }
        }
    }
}
