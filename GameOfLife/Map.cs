﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Map
    {
        public bool[,] cells  { get; set; }
        public int size { get; set; }
        public Map(int size)
        {
            this.size = size;
            cells = new bool[size, size];
        }

        public void Fill()
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (cells[i, j] == false)
                    {
                        Console.Write('.');
                    }
                    else
                    {
                        Console.Write('*');
                    }
                }
                Console.WriteLine();
            }
        }

        private int Neighbour(int x, int y)
        {
            int neighbour = 0;

            if (x != size - 1)
            {
                if (cells[x+1,y]== true)
                {
                    neighbour++;
                }
            }

            if (x != size - 1 && y != size - 1)
            {
                if (cells[x + 1, y + 1] == true)
                {
                    neighbour++;
                }
            }

            if (y != size - 1)
            {
                if (cells[x, y + 1] == true)
                {
                    neighbour++;
                }
            }
            
            if (x != 0 && y != size - 1)
            {
                if (cells[x - 1, y + 1] == true)
                {
                    neighbour++;
                }
            }
            
            if (x != 0)
            {
                if (cells[x - 1, y] == true)
                {
                    neighbour++;
                }
            }
            
            if (x != 0 && y != 0)
            {
                if (cells[x - 1, y - 1] == true)
                {
                    neighbour++;
                }
            }
            
            if (y != 0)
            {
                if (cells[x, y - 1] == true)
                {
                    neighbour++;
                }
            }
            
            if (x != size - 1 && y != 0)
            {
                if (cells[x + 1, y - 1] == true)
                {
                    neighbour++;
                }
            }


            return neighbour;
        }

        private bool ConsequencesOfNeighbours(int x, int y)
        {
            int neighbours = Neighbour(x, y);

            if(cells[x,y] == true)
            {
                if (neighbours < 2 || neighbours > 3)
                {
                    return false;
                }
                if (neighbours == 2 || neighbours == 3)
                {
                    return true;
                }
            }
            if (cells[x,y] == false)
            {
                if(neighbours == 3)
                {
                    return true;
                }
            }
            return false;
        }


        public void Play()
        {
            bool[,] newCells = new bool[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    newCells[i, j] = ConsequencesOfNeighbours(i, j);
                }
            }
            cells = newCells;
        }

    }
}
